const Menu = require("electron").Menu;
const config = require("../../vue.config");
import { default as Template } from "../appMenu";
import i18n from "../configs/i18next.config";

const menu = null;
const platform = config.pluginOptions.platform;

function MenuFactoryService(menu) {
  this.menu = menu;
  this.buildMenu = buildMenu;
}

function buildMenu(app, mainWindow, i18n) {
  if (config.pluginOptions.platform === "darwin") {
    this.menu = Menu.buildFromTemplate(darwinTemplate(app, mainWindow, i18n));
    Menu.setApplicationMenu(this.menu);
  } else {
    this.menu = Menu.buildFromTemplate(Template(app, mainWindow, i18n));
    Menu.setApplicationMenu(this.menu);
  }
}

let menuFactoryService = new MenuFactoryService(menu);

export default menuFactoryService;
