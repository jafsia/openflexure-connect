import i18next from "./i18next.config";
import VueI18Next from "@panter/vue-i18next";
import Vue from "vue";
const config = require("../../vue.config");

const i18nextOptions = {
  backend: {
    // path where resources get loaded from
    loadPath: "./src/locales/{{lng}}/{{ns}}.json",

    // path to post missing resources
    addPath: "./src/locales/{{lng}}/{{ns}}.missing.json",

    // jsonIndent to use when storing json files
    jsonIndent: 2
  },
  interpolation: {
    escapeValue: false
  },
  lng: "en",
  saveMissing: true,
  fallbackLng: config.pluginOptions.fallbackLng,
  whitelist: config.pluginOptions.languages,
  react: {
    wait: false
  }
};
Vue.use(VueI18Next);

// initialize if not already initialized
if (!i18next.isInitialized) {
  i18next.init(i18nextOptions);
}
const i18n = new VueI18Next(i18next);
export default i18n;
