import i18n from "i18next";
const i18nextBackend = require("i18next-node-fs-backend");
const config = require("../../vue.config");

const i18nextOptions = {
  backend: {
    // path where resources get loaded from
    loadPath: "./src/locales/{{lng}}/{{ns}}.json",

    // path to post missing resources
    addPath: "./src/locales/{{lng}}/{{ns}}.missing.json",

    // jsonIndent to use when storing json files
    jsonIndent: 2
  },
  interpolation: {
    escapeValue: false
  },
  saveMissing: true,
  fallbackLng: config.pluginOptions.fallbackLng,
  whitelist: config.pluginOptions.languages,
  react: {
    wait: false
  }
};
i18n.use(i18nextBackend);

// initialize if not already initialized
if (!i18n.isInitialized) {
  i18n.init(i18nextOptions);
}

export default i18n;
