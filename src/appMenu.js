/* global __static */

import { app, shell, Menu } from "electron";
const updater = require("electron-updater");
const autoUpdater = updater.autoUpdater;
const config = require("../vue.config");
const path = require("path");
// const openAboutWindow = require("about-window").default;
import { default as openAboutWindow } from "about-window";
// import i18n from './configs/i18next.config';

export default function(app, mainWindow, i18n) {
  let template = [
    {
      label: i18n.t("edit"),
      submenu: [
        
        { label: i18n.t("Undo"), role: "undo" },
        { label: i18n.t("Redo"), role: "redo" },
        { type: "separator" },
        { label: i18n.t("Cut"),  role: "cut" },
        { label: i18n.t("Copy"), role: "copy" },
        { label: i18n.t("Paste"), role: "paste" },
        { label: i18n.t("delete"), role: "delete" },
        { label: i18n.t("Select All"), role: "selectAll" },
        
      ]
    },
    {
      label: i18n.t("view"),
      submenu: [
        { label: i18n.t("Reload"),role: "reload" },
        { label: i18n.t("force reload"),role: "forcereload" },
        { label: i18n.t("Toggle Developer Tools"),role: "toggledevtools" },
        { type: "separator" },
        { label:i18n.t("Toggle Full Screen"),role: "togglefullscreen" }
      ]
    },
    {
      role: "window",
      submenu: [{ label: i18n.t("minimize"), role: "minimize" }, { label: i18n.t("close"),role: "close" }]
    },
    {
      label: i18n.t("help"), 
      role: "help",
      submenu: [
        {
          label: i18n.t("About"),
          click: () =>
            openAboutWindow({
              icon_path: path.join(__static, "icon.png"),
              about_page_dir: path.join(__static, "about"),
              package_json_dir: __dirname,
              use_version_info: true,
              homepage: "https://gitlab.com/openflexure/openflexure-connect",
              bug_report_url:
                "https://gitlab.com/openflexure/openflexure-connect/issues",
              license: "GNU General Public License v3.0"
            })
        },
        {
          label: i18n.t("report an issue"),
          click() {
            shell.openExternal(
              "https://gitlab.com/openflexure/openflexure-connect/issues"
            );
          }
        },
        { type: "separator" },
        {
          label: i18n.t("check for updates"),
          click() {
            autoUpdater.checkForUpdates();
          }
        }
      ]
    }
  ];

  if (process.platform === "darwin") {
    template.unshift({
      label: app.getName(),
      submenu: [
        { label: i18n.t("About"),role: "about" },
        { type: "separator" },
        { label: i18n.t("services"),role: "services" },
        { type: "separator" },
        { label: i18n.t("hide"),role: "hide" },
        { label: i18n.t("hideothers"),role: "hideothers" },
        { label: i18n.t("unhide"),role: "unhide" },
        { type: "separator" },
        { label: i18n.t("Quit"),role: "quit" }
      ]
    });

    // Edit menu
    template[1].submenu.push(
      { type: "separator" },
      {
        label: i18n.t("Speech"),
        submenu: [{ label: i18n.t("startspeaking"),role: "startspeaking" }, { label: i18n.t("stopspeaking"),role: "stopspeaking" }]
      }
    );

    // Window menu
    template[3].submenu = [
      { label: i18n.t("close"),role: "close" },
      { label: i18n.t("minimize"),role: "minimize" },
      { label: i18n.t("zoom"),role: "zoom" },
      { type: "separator" },
      { label: i18n.t("front"),role: "front" }
    ];
  } else {
    template.unshift({
      label: i18n.t("file"),
      submenu: [{label: i18n.t("exit"), role: "quit" }]
    });
  }

  const languageMenu = config.pluginOptions.languages.map(languageCode => {
    return {      
      label: languageCode,
      type: "radio",
      checked: i18n.language === languageCode,
      load: () => {
        i18n.changeLanguage("en");
      },
      click: () => {
        i18n.changeLanguage(languageCode);
      }
    };
  });

  template.push({
    label: i18n.t("language") ,
    submenu: languageMenu
  });

  return template;
}
